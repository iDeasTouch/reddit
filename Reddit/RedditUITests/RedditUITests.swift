//
//  RedditUITests.swift
//  RedditUITests
//
//  Created by Gustavo Halperin on 12/5/18.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

import XCTest

class RedditUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        let app = XCUIApplication()
        let cellsQuery = app.collectionViews.cells
        
        let pictureBlock = { () in
            let saveButton = app.buttons["Save"]
            saveButton.tap()
            let saveImageAlert = app.alerts["Save Image"]
            saveImageAlert.buttons["Cancel"].tap()
            saveButton.tap()
            saveImageAlert.buttons["Ok"].tap()
            saveButton.tap()
            app.buttons["Close"].tap()
        }
        let iterateCells = { () -> Bool in
            // cellsQuery.element(boundBy: 1).descendants(matching: .image).matching(identifier: "Thumbnail Default")
            // cellsQuery.element(boundBy: 2).descendants(matching: .image).debugDescription.contains("Thumbnail Default")
            for i in 0..<cellsQuery.count {
                let element = cellsQuery.element(boundBy: i)
                if element.descendants(matching: .image).debugDescription.contains("Thumbnail Default") == false {
                    element.tap()
                    pictureBlock()
                    return true
                }
            }
            return false
        }
        cellsQuery.element(boundBy: 0).swipeDown()
        cellsQuery.element(boundBy: 0).swipeDown()
        cellsQuery.element(boundBy: 0).swipeDown()
        if iterateCells() == true { return }
        cellsQuery.element(boundBy: 0).swipeUp()
        if iterateCells() == true { return }
        cellsQuery.element(boundBy: 0).swipeUp()
        if iterateCells() == true { return }
        cellsQuery.element(boundBy: 0).swipeUp()
        if iterateCells() == true { return }
        
        
        /*
        for i in 0..<cellsQuery.count {
            if cellsQuery.element(boundBy: i).isHittable {
                
            }
        }
        cellsQuery.otherElements.containing(.staticText, identifier:"FritzBakon").children(matching: .textView).element.swipeUp()
        
        let fritzbakonElement = cellsQuery.otherElements.containing(.staticText, identifier:"FritzBakon").element
        fritzbakonElement/*@START_MENU_TOKEN@*/.press(forDuration: 1.1);/*[[".tap()",".press(forDuration: 1.1);"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        
        let closeButton = app.buttons["Close"]
        closeButton.tap()
        fritzbakonElement/*@START_MENU_TOKEN@*/.press(forDuration: 1.0);/*[[".tap()",".press(forDuration: 1.0);"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        closeButton.tap()
        
        let wigbillyElement = cellsQuery.otherElements.containing(.staticText, identifier:"WigBilly_").element
        wigbillyElement.tap()
        wigbillyElement.tap()
        fritzbakonElement.tap()
        closeButton.tap()
               
        */
        
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

}
