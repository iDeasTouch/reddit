//
//  RedditCollectionViewController.swift
//  Reddit
//
//  Created by Gustavo Halperin on 11/18/18.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

import Foundation
import UIKit

/**
 class RedditCollectionViewController
 Custom UICollectionViewController with RedditCollectionViewLayout as his custom UICollectionViewLayout.
 
 Main functions:
 #. Keep current RedditData model saved in UserDefaul.
 #. Check current RedditData model in each apear instead of always fetch data from server.
 #. Save current visible collection view index before rotation and scroll to it after rotation.
 #. Save index of selectd collection view on selection and scroll when user back to collection view without matter if the device have or nor
   different orientation.
 #. Always save index of the current vissible (unless there is a collection view selected) and restore this position in each new did appear.
 #. Refresh data with the first 50 top post on swipe down from top.
 #. Fetch the next 50 top messages when the the current last Collection View is about to be visible.
 
 Patterns:
 MVC: View    <=> Controller                      <=> Model
      NibFile <=> RedditCollectionViewController  <=> RedditCollectionViewLayout
     The NibFile share his view responsabilities with he RedditCollectionViewLayout to decide the postion and size of each
      Collection View.
 */

class RedditCollectionViewController: UICollectionViewController {
    static fileprivate let collectionViewCellReuseIdentifier = "RedditCollectionViewCell"
    static fileprivate let collectionViewCellIdentifier = "RedditCollectionViewCellIdentifier"
    static fileprivate let redditModelIdentifier = "RedditCollectionViewController-RedditModel"
    static fileprivate let currentIndexIdentifier = "RedditCollectionViewController-CurrentIndex"
    
    var redditModel: RedditModel? {
        get {
            guard let data = UserDefaults.standard.object(forKey: RedditCollectionViewController.redditModelIdentifier) as? Data else {
                return nil
            }
            return try? JSONDecoder().decode(RedditModel.self, from: data)
        }
        set {
            guard let newValue = newValue else {
                UserDefaults.standard.set(nil, forKey: RedditCollectionViewController.redditModelIdentifier)
                return
            }
            guard let data = try? JSONEncoder().encode(newValue) else {
                UserDefaults.standard.set(nil, forKey: RedditCollectionViewController.redditModelIdentifier)
                return
            }
            UserDefaults.standard.set(data, forKey: RedditCollectionViewController.redditModelIdentifier)
        }
    }
    var currentIndex: Int? {
        get {
            return UserDefaults.standard.object(forKey: RedditCollectionViewController.currentIndexIdentifier) as? Int
        }
        set {
            UserDefaults.standard.set(newValue, forKey: RedditCollectionViewController.currentIndexIdentifier)
        }
    }
    
    @IBOutlet weak var unAuthorizedImage: UIImageView!
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let redditCollectionViewLayout = self.collectionViewLayout as? RedditCollectionViewLayout,
            let count = redditCollectionViewLayout.redditModel?.childrenList.count else { return 0 }
        return count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: RedditCollectionViewController.collectionViewCellReuseIdentifier, for: indexPath)
        guard let redditCollectionViewCell = collectionViewCell as? RedditCollectionViewCell,
            let redditCollectionViewLayout = self.collectionViewLayout as? RedditCollectionViewLayout else {
                return collectionViewCell
        }
        redditCollectionViewCell.redditChildren = redditCollectionViewLayout.redditModel?.childrenList[indexPath.row]
        let count = self.collectionView(collectionView, numberOfItemsInSection: indexPath.section)
        if count > 0, indexPath.row == (count - 1) { self.fetchData() }
        if let items = collectionView.indexPathsForSelectedItems, items.count > 0 {
            return redditCollectionViewCell
        }
        if collectionView.indexPathsForVisibleItems.count > 0 {
            self.currentIndex = collectionView.indexPathsForVisibleItems[0].row
        }
        else {
            self.currentIndex = nil
        }
        return redditCollectionViewCell
    }
    
    private func fetchData() {
        self.collectionView.refreshControl?.endRefreshing()
        ShowRedditActivityIndicator()
        let after = (self.collectionViewLayout as? RedditCollectionViewLayout)?.redditModel?.after
        RedditNetworkManager.shared.topPost(after: after) { (json:Any?, error:Error?) in
            guard let json = json,
                let afterRedditModel = RedditModel(json),
                let redditCollectionViewLayout = self.collectionViewLayout as? RedditCollectionViewLayout else {
                    HideRedditActivityIndicator()
                    return
            }
            if let redditModel = redditCollectionViewLayout.redditModel {
                redditModel.union(redditModel: afterRedditModel)
            }
            else {
                redditCollectionViewLayout.redditModel = afterRedditModel
            }
            self.redditModel = redditCollectionViewLayout.redditModel
            self.collectionView.reloadData()
            HideRedditActivityIndicator()
        }
    }
    
    @objc  func refreshData() {
        self.redditModel = nil
        self.currentIndex = nil
        (self.collectionViewLayout as? RedditCollectionViewLayout)?.redditModel = nil
        self.fetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.unAuthorizedImage.isHidden = RedditNetworkManager.shared.isAuthorized
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let currentIndexInAppear = self.currentIndex
        let scrollToCurrent = { () in
            if let currentIndex = currentIndexInAppear {
                self.collectionView.scrollToItem(at: IndexPath(row: currentIndex, section: 0), at: .top, animated: true)
            } }
        let fetchDataLocally =  { (_ redditModel:RedditModel, _ redditCollectionViewLayout: RedditCollectionViewLayout) in
            ShowRedditActivityIndicator()
            redditCollectionViewLayout.redditModel = redditModel
            self.collectionView.reloadData()
            HideRedditActivityIndicator()
            scrollToCurrent()
        }
        guard RedditNetworkManager.shared.isAuthorized else {
            UIViewController.currentViewController()?.presentAlertViewWithAction(
                "Reddit Authorization",
                message: "Looks like we have not authorization to see Reddit's posts. Please click OK and gives us Authorization.",
                actionTitle: "Ok",
                action: { self.performSegue(withIdentifier: "RedditToAuthorizationSegue", sender: self) })
            return
        }
        if self.collectionView.refreshControl != nil {
            scrollToCurrent()
            return
        }
        if let redditModel = self.redditModel,
            let redditCollectionViewLayout = self.collectionViewLayout as? RedditCollectionViewLayout {
            fetchDataLocally(redditModel,  redditCollectionViewLayout)
        }
        else {
            self.fetchData()
        }
        self.collectionView.refreshControl = UIRefreshControl()
        self.collectionView.refreshControl?.addTarget(
            self,
            action: #selector(RedditCollectionViewController.refreshData),
            for: .valueChanged)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        let items =  self.collectionView.indexPathsForVisibleItems
        if items.count > 0 {
            let currentIndex = items[0].row
            coordinator.animate(alongsideTransition: nil) { (_:UIViewControllerTransitionCoordinatorContext) in
                guard let redditCollectionViewLayout = self.collectionViewLayout as? RedditCollectionViewLayout else { return }
                let rect = redditCollectionViewLayout.itemFrame(index:currentIndex)
                self.collectionView.scrollRectToVisible(rect, animated: true)
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.currentIndex = indexPath.row
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier,
            identifier == "ItemToPictureSegue",
            let destination = segue.destination as? PictureViewController,
            let url = (sender as? RedditCollectionViewCell)?.redditChildren?.url else { return }
        destination.url = url
    }
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return (sender as? RedditCollectionViewCell)?.redditChildren?.url != nil
    }
}
