//
//  PictureViewController.swift
//  Reddit
//
//  Created by Gustavo Halperin on 11/18/18.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

import Foundation
import UIKit

/**
 class PictureViewController
 ViewController with Image that is loaded in realtime and option to save it in Photo Album.
 */

class PictureViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    var url:String? {  didSet { if self.isViewLoaded { self.loadImage() } } }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadImage()
    }
    
    private func loadImage() {
        guard let urlStr = self.url, let url = URL(string: urlStr) else { return }
        ShowRedditActivityIndicator()
        URLSession.shared.dataTask(with: url) { (data:Data?, response:URLResponse?, error:Error?) in
            DispatchQueue.main.async {
                HideRedditActivityIndicator()
                guard error == nil, let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                    print("Not a proper HTTPURLResponse or statusCode")
                    return
                }
                self.imageView.image = UIImage(data: data!)
            }
            }.resume()
    }
    
    @objc func completionSaveImage(_ image:UIImage, _ didFinishSavingWithError:Error?, _ contextInfo: Any?) {
        HideRedditActivityIndicator()
        let message = didFinishSavingWithError == nil ?
            "The image was saved in your Photos Album." : "We had a problem saving this image. Please try agaim later."
        UIViewController.currentViewController()?
            .presentAlertViewWithVoidAction("Save Image",  message: message, voidTitle: "OK")
    }
    
    @IBAction func saveImage(_ sender: Any) {
        guard let image = self.imageView.image else { return }
        self.presentAlertViewWithActionAndCancel(
            "Save Image",
            message: "Would you like to save this image in you Photos Album") {
                ShowRedditActivityIndicator()
                UIImageWriteToSavedPhotosAlbum(image, self, #selector(PictureViewController.completionSaveImage(_:_:_:)), nil)
        }
    }
}
