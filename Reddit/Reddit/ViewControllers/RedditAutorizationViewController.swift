//
//  RedditAutorizationViewController.swift
//  Reddit
//
//  Created by Gustavo Halperin on 11/22/18.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

import Foundation
import WebKit

/**
 class RedditAutorizationViewController.
 ViewController with a WKWebView to handle authorization process without shows open URL with our private Reddit ID.
 */

class RedditAutorizationViewController: UIViewController, WKNavigationDelegate {
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.navigationDelegate = self
        
        if let url = URL(string: RedditNetworkManager.shared.oauth2URL) {
            let urlRequest = URLRequest(url: url)
            self.webView.load(urlRequest)
        }
    }
    
    // WKNavigationDelegate
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let url = webView.url?.absoluteString else {
            decisionHandler(.allow)
            return
        }
        RedditNetworkManager.shared.proccessAutorizationForNavigationAction(url: url, wkNavigationAction: navigationAction)
        { (navigationActionPolicy:WKNavigationActionPolicy?, erro:Error?) in
            guard let actionPolicy = navigationActionPolicy else {
                decisionHandler(.allow)
                return
            }
            switch actionPolicy {
            case .allow:
                decisionHandler(.allow)
            case .cancel:
                self.backAction()
                decisionHandler(.cancel)
            }
        }
    }
}
