//
//  RedditCollectionViewCell.swift
//  Reddit
//
//  Created by Gustavo Halperin on 11/23/18.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

import Foundation
import UIKit

/**
 class RedditCollectionViewCell
 Typical UICollectionViewCell included implementation of prepareForReuse
 */

class RedditCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var comments: UILabel!
    @IBOutlet weak var date: UILabel!
    
    private func updateLabel(_ label:UILabel, _ text:String?) {
        guard let text = text else {
            label.attributedText = nil
            return
        }
        label.attributedText = NSAttributedString(.Tag, text:text)
    }
    
    private func updateTextView(_ textView:UITextView, _ text:String?) {
        guard let text = text else {
            textView.attributedText = nil
            return
        }
        textView.attributedText = NSAttributedString(.Title, text:text)
    }
    
    var redditChildren:RedditChildren? { didSet {
        guard let children = self.redditChildren else {
            self.prepareForReuse()
            return
        }
        self.updateLabel(self.author, children.author)
        self.updateTextView(self.textView, children.title)
        self.updateLabel(self.comments, String(children.numComments, postPendOne: "message", postPendMany: "messages"))
        self.updateLabel(self.date, String(timeAgo: TimeInterval(children.created)))
        
        if let urlStr = children.url, let url = URL(string: urlStr) {
            URLSession.shared.dataTask(with: url) {
                (data:Data?, response:URLResponse?, error:Error?) in
                guard error == nil, (response as? HTTPURLResponse)?.statusCode == 200 else { return }
                DispatchQueue.main.async { self.thumbnail.image = UIImage(data: data!) }
                }.resume()
        } }
    }
    
    override func prepareForReuse() {
        self.thumbnail.image = UIImage(named: "Thumbnail Default")
        self.author.attributedText = nil
        self.textView.attributedText = nil
        self.comments.attributedText = nil
        self.date.attributedText = nil
    }
}
