//
//  RedditCollectionViewLayout.swift
//  Reddit
//
//  Created by Gustavo Halperin on 11/25/18.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

import UIKit

/**
 class RedditCollectionViewLayout
 Custom UICollectionViewLayout to calculate the content sizes and their locations depend on the device screen and orientation.
 */

class RedditCollectionViewLayout: UICollectionViewLayout {
    var redditModel:RedditModel?
    
    private var computedContentSize: CGSize = .zero
    
    private var cellAttributes = [IndexPath: UICollectionViewLayoutAttributes]()
    
    private func indexToCoordinates(index:Int) ->  (col:CGFloat, row:CGFloat, side:CGFloat) {
        let size = UIScreen.main.bounds.size
        let factor = size.width > size.height ? 2 : 1
        let col = CGFloat(index % factor)
        let row = CGFloat(index / factor)
        let side = size.width / CGFloat(factor)
        return (col, row, side)
    }
    
    func itemFrame(index:Int) -> CGRect {
        let (col, row, side) = indexToCoordinates(index: index)
        let originX = side * col
        let originY = side * row
        return CGRect(x: originX, y: originY, width: side, height: side)
    }
    
    override func prepare() {
        let size = UIScreen.main.bounds.size
        self.cellAttributes = [IndexPath: UICollectionViewLayoutAttributes]()
        guard let redditModel = self.redditModel else { return }
        let count = redditModel.childrenList.count
        if size.width > size.height {
            self.computedContentSize = CGSize(width: size.width, height: size.height * CGFloat(count / 2) )
        }
        else {
            self.computedContentSize = CGSize(width: size.width, height: size.width * CGFloat(count) )
        }
        var indexPath = IndexPath(item: 0, section: 0)
        for item in 0..<count {
            indexPath.row = item
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = self.itemFrame(index: item)
            cellAttributes[indexPath] = attributes
        }
    }
    
    override var collectionViewContentSize: CGSize {
        return self.computedContentSize
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var attributeList = [UICollectionViewLayoutAttributes]()
        
        for (_, attributes) in cellAttributes {
            if attributes.frame.intersects(rect) {
                attributeList.append(attributes)
            }
        }
        
        return attributeList
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return self.cellAttributes[indexPath]
    }
}

