//
//  Foundation+Ext.swift
//  Reddit
//
//  Created by Gustavo Halperin on 11/25/18.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

import Foundation


/**
 File Foundation+Ext.swift
 #. Extension of DispatchQueue to support block.
 #. Extension NSObject to support init by dictionary using keys and values.
 */

func dispatch_async_main(_ block: @escaping ()->()) {
    DispatchQueue.main.async(execute: DispatchWorkItem(block:block))
}

extension NSObject {
    convenience init( attributeDictionary:NSDictionary) {
        self.init()
        for (key, value) in attributeDictionary {
            if let string = key as? String {
                self.setValue(value, forKey:string)
            }
        }
    }
    
    convenience init( attributeDictionary:[String:Any?]) {
        self.init()
        for (key, value) in attributeDictionary {
            self.setValue(value, forKey:key)
        }
    }
}
