//
//  String+Ext.swift
//  Reddit
//
//  Created by Gustavo Halperin on 11/24/18.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

import Foundation
import UIKit

/**
 File String+Ext.swift
 Decorators to Int, String, and NSAttributedString later on used in RedditCollectionViewCell fields.
 */


fileprivate var OneDecimalFormatter:NumberFormatter = NumberFormatter(attributeDictionary: ["maximumFractionDigits":1])

fileprivate func NumberToString(_ num:Int, _ postPendOne:String, _ postPendMany:String) -> String? {
    var messages = ""
    if num >= 1_000 {
        let fractional = NSNumber(value: Double(num)/1_000)
        guard let prepend = OneDecimalFormatter.string(from: fractional) else {
            return nil
        }
        messages = "\(prepend)k"
    }
    else {
        messages = String(num)
    }
    return num == 1 ? "\(messages) \(postPendOne)" : "\(messages) \(postPendMany)"
}

extension String {
    init?(_ num:Int, postPendOne:String, postPendMany:String) {
        guard let numStr = NumberToString(num, postPendOne, postPendMany) else { return nil }
        self = numStr
    }
    init?(timeAgo:TimeInterval) {
        let calendar = Calendar.init(identifier: .gregorian)
        let dateAgo = Date(timeIntervalSince1970: timeAgo)
        let components = calendar.dateComponents([.hour, .day], from: dateAgo, to: Date())
        guard let days = components.day, let hours = components.hour else { return nil }
        if days == 0, hours == 0 {
            self = "Just now"
            return
        }
        var daysInterval = ""
        if days > 0 {
            switch days {
            case 1:
                daysInterval = "One day "
            case let d where d < 31:
                daysInterval = "\(d) days"
            default:
                self = "More than one month ago."
                return
            }
        }
        switch hours {
        case 0:
            self = "\(daysInterval) ago."
        case 1:
            self = days > 0 ? "\(daysInterval) and one hour ago." : "One hour ago."
        default:
            self = days > 0 ? "\(daysInterval) and \(hours) hours ago." : "\(hours) hours ago."
        }
    }
}

extension NSAttributedString {
    enum PostField { case Tag, Title }
    convenience init?(_ field: PostField, text: String) {
        let titleAttributesBlock = {
            (font:UIFont, backgroundColor:UIColor, strokeColor:UIColor) ->  [NSAttributedString.Key : Any]? in
            return [NSAttributedString.Key.font: font,
                    NSAttributedString.Key.backgroundColor: backgroundColor,
                    NSAttributedString.Key.strokeColor: strokeColor ]
        }
        switch field {
        case .Tag:
            self.init(string: text,
                      attributes: titleAttributesBlock(UIFont.redditTag, UIColor.redditTagBackground, UIColor.redditTagStroke))
        case .Title:
            guard let font = UIFont.redditTitle else { return nil }
            self.init(string: text,
                      attributes: titleAttributesBlock(font, UIColor.redditTitleBackground, UIColor.redditTitleStroke))
        }
    }
}
