//
//  RedditNetworkManager.swift
//  Reddit
//
//  Created by Gustavo Halperin on 11/22/18.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

import Foundation
import WebKit

/**
 Struct RedditData
 Has the Reddit App values, as clientId. Please replace them by your and update your app with the same redirect URL.
 This is a fileprivate struc basicaly build the URL, and the entired reques.
 
 Patterns:
 #. Enum with values inside Switch.
 #. Custom serialization (Array.reduce) because a bug on Server's Reddit.
 */

/**
 class RedditNetworkManager
 This class have the next main functionalities:
 #. Assist RedditAuthorizationViewController in his steps to get authorization to Reddit server.
 #. Save the token obtainer with RedditAuthorizationViewController, the expiration date and the refresh token in UserDefault.
 #. Resolve autocamaticall expiration situation refreshing the tpken and calling recursivelly the initial function.
 #. fetch the first 50 top post starting by the after parameter.
 
 Patterns:
 #. Singlenton
 #. Computer Variables
 #. Recursion.
 */


fileprivate struct RedditData {
    let clientId = "5PrG8OCStcAS0w"
    let redirect =  "Reddit://response" // configure your redirect with the same one here.
    let baseURL = "https://www.reddit.com"
    let baseAthorizasedURL = "https://oauth.reddit.com"
    private let accessTokenPostURL = "/api/v1/access_token"
    
    var accessTokenURL:String { get { return self.baseURL + self.accessTokenPostURL } }
    var oauth2URL:String { get {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let length = letters.count
        let randomString = String((0...length-1).map{ _ in letters.randomElement()! })
        let parameters = ["client_id": self.clientId,
                          "response_type": "code",
                          "redirect_uri":self.redirect,
                          "scope":"read",
                          "state":randomString,
                          "duration":"permanent"]
        var request = parameters.reduce("\(self.baseURL)/api/v1/authorize?") { "\($0)\($1.0)=\($1.1)&" }
        request.removeLast()
        return request
        }}
    
    private func buildAuthorizationHttpBody(code:String) -> Data? {
        let parameters = ["grant_type": "authorization_code",
                          "code": code,
                          "redirect_uri": self.redirect,
                          "client_id":self.clientId,
                          "client_secret":"",
                          "uration":"permanent"]
        // Used custom serialization because Reddit doesn't support JSON Serialization.
        var parametersSerialization = parameters.reduce("") { "\($0)\($1.0)=\($1.1)&" }
        parametersSerialization.removeLast()
        return parametersSerialization.data(using: String.Encoding.utf8)
    }
    private func buildRefreshTokenHttpBody(refreshToken:String) -> Data? {
        let parameters = ["client_id":self.clientId,
                          "client_secret":"",
                          "grant_type": "refresh_token",
                          "refresh_token": refreshToken]
        // Used custom serialization because Reddit doesn't support JSON Serialization.
        var parametersSerialization = parameters.reduce("") { "\($0)\($1.0)=\($1.1)&" }
        parametersSerialization.removeLast()
        return parametersSerialization.data(using: String.Encoding.utf8)
    }
    
    enum AuthorizationRequestParam {
        case Code(value:String)
        case RefreshToken(value:String)
    }
    func buildAuthorizationRequest(_ authorizationRequestParam:AuthorizationRequestParam) -> URLRequest? {
        guard let url = URL(string: self.accessTokenURL) else {
            return nil
        }
        let authentification = "\(self.clientId):".data(using: String.Encoding.utf8)
        guard let base64Encoded = authentification?.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0)) else {
            return nil
        }
        var httpBody:Data?
        switch authorizationRequestParam {
        case let .Code(value):
            httpBody = self.buildAuthorizationHttpBody(code: value)
        case let .RefreshToken(value):
            httpBody = self.buildRefreshTokenHttpBody(refreshToken: value)
        }
        guard let _ = httpBody else {
            return nil
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = httpBody
        request.addValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
        return request
    }
}

/**
 */
class RedditNetworkManager {
    static private let redditData = RedditData()
    static let shared = RedditNetworkManager()
    
    var oauth2URL: String { get { return RedditNetworkManager.redditData.oauth2URL }}
    
    var isAuthorized: Bool { get { return self.token != nil } }
    
    var isTokenExpired: Bool? { get {
        guard let authorization = self.authorization,
            let expires_in = authorization["expires_in"] as? Date else {
                return nil
        }
        return expires_in < Date()
        } }
    
    private var token: String? {
        get {
            guard let authorization = self.authorization else { return nil }
            guard let access_token = authorization["access_token"] as? String,
                let _ = authorization["refresh_token"] as? String else {
                    return nil
            }
            return access_token
        }
    }
    
    private var authorization: [String:Any]? {
        get {
            return UserDefaults.standard.dictionary(forKey: "RedditAuthorization\(RedditNetworkManager.redditData.clientId)")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "RedditAuthorization\(RedditNetworkManager.redditData.clientId)")
        }
    }
    
    func buildAuthorizaztion(authorizationResponse:[String:Any]) -> [String:Any]? {
        guard let timeInterval = authorizationResponse["expires_in"] as? TimeInterval,
            let access_token = authorizationResponse["access_token"] as? String,
            let refresh_token = authorizationResponse["refresh_token"] as? String else {
            return nil
        }
        let expires_in = Date(timeIntervalSinceNow: timeInterval)
        return ["access_token":access_token, "refresh_token": refresh_token, "expires_in": expires_in]
    }
    
    func updateAuthorizaztion(authorizationResponse:[String:Any]) -> [String:Any]? {
        guard let timeInterval = authorizationResponse["expires_in"] as? TimeInterval,
            let access_token = authorizationResponse["access_token"] as? String,
            let refresh_token = self.authorization?["refresh_token"] as? String else {
                return nil
        }
        let expires_in = Date(timeIntervalSinceNow: timeInterval)
        return ["access_token":access_token, "refresh_token": refresh_token, "expires_in": expires_in]
    }
    
    private func codeForNavigationAction( url: String, wkNavigationAction: WKNavigationAction) -> String? {
        guard let requestUrl = wkNavigationAction.request.url?.absoluteString,
            requestUrl.lowercased().hasPrefix(RedditNetworkManager.redditData.redirect.lowercased()) else {
                return nil
        }
        let urlElements = url.components(separatedBy: "state=")
        assert(urlElements.count == 2, "Were find only one state on url: \(url)")
        let tokenLements = urlElements[1].components(separatedBy: "&code=")
        assert(tokenLements.count == 2, "Were find only one element at codel")
        return tokenLements[1]
    }
    
    func proccessAutorizationForNavigationAction(
        url: String, wkNavigationAction: WKNavigationAction, complete: @escaping((WKNavigationActionPolicy?,Error?)->Void) ) {
        guard let code = self.codeForNavigationAction(url: url, wkNavigationAction: wkNavigationAction),
            let request = RedditNetworkManager.redditData.buildAuthorizationRequest(.Code(value: code)) else {
            complete(nil, nil)
            return
        }
        let completeMainQueue = { (_ policy:WKNavigationActionPolicy?,_ error:Error?) in
            DispatchQueue.main.async { complete(policy, error) }
        }
        URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil,
                let data = data else {
                completeMainQueue(nil, error)
                return
            }
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    guard let authorization = self.buildAuthorizaztion(authorizationResponse: json) else {
                        completeMainQueue(nil, nil)
                        return
                    }
                    self.authorization = authorization
                    completeMainQueue(.cancel, nil)
                    return
                }
            } catch let error {
                completeMainQueue(nil, error)
                return
            }
        }).resume()
    }
    
    private func refreshTokenAction(complete:@escaping ((String?, Error?)->Void)) {
        guard let authorization = self.authorization,
            let refresh_token = authorization["refresh_token"] as? String,
            let request = RedditNetworkManager.redditData.buildAuthorizationRequest(.RefreshToken(value: refresh_token)) else {
                self.authorization = nil
                complete(nil, nil)
                return
        }
        URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil,
                let data = data else {
                    self.authorization = nil
                    complete(nil, error)
                    return
            }
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    guard let authorization = self.updateAuthorizaztion(authorizationResponse: json) else {
                        self.authorization = nil
                        complete(nil, nil)
                        return
                    }
                    self.authorization = authorization
                    complete(self.token, nil)
                    return
                }
            } catch let error {
                self.authorization = nil
                complete(nil, error)
                return
            }
        }).resume()
    }
}

extension RedditNetworkManager {
    func topPost(limit:Int = 50, after:String?, complete:@escaping( (Any?, Error?)->Void )) {
        guard let token = RedditNetworkManager.shared.token else {
            complete(nil,  nil)
            return
        }
        if let isTokenExpired = self.isTokenExpired, isTokenExpired {
            self.refreshTokenAction { (_:String?, _:Error?) in
                self.topPost(limit:limit, after: after, complete: complete)
            }
            return
        }
        var urlStr = "\(RedditNetworkManager.redditData.baseAthorizasedURL)/top?limit=limit"
        if let after = after {
            urlStr += "&after=\(after)"
        }
        guard let url = URL(string: urlStr) else {
            complete(nil,  nil)
            return
        }
        let completeMainQueue = { (_ any:Any?,_ error:Error?) in
            DispatchQueue.main.async { complete(any, error) }
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil,
                let data = data else {
                    completeMainQueue(nil, error)
                    return
            }
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    completeMainQueue(json, nil)
                }
            } catch let error {
                completeMainQueue(nil, error)
            }
        }).resume()
    }
}
