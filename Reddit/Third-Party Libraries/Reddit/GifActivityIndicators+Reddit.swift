//
//  GifActivityIndicators+Reddit.swift
//  Reddit
//
//  Created by Gustavo Halperin on 11/25/18.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

func ShowRedditActivityIndicator() {
    ShowGifActivityIndicator("Activity Indicator")
}

func HideRedditActivityIndicator() {
    HideGifActivityIndicator("Activity Indicator")
}
