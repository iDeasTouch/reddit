//
//  RedditModel.swift
//  Reddit
//
//  Created by Gustavo Halperin on 11/23/18.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

import Foundation

/**
 File RedditModel.swift
 In this file is defined the object representing the reddit data objecs.
 The Data structure of an object returned when using the reddit JSON API are in the link below:
   https://github.com/reddit-archive/reddit/wiki/JSON
 #. Custom Model representing the values required to show values required in RedditCollectionViewCell & PictureViewController.
 #. s
 #. All the Classes inherit from Codable so the entirable model can be encoded and saved in DefaultUser and decoder when is read.
 
 Patterns:
 #. Custom build from Any?.
 #. Encode and Decode using Codable protocol.
 */


class RedditSource: Codable {
    let url: String
    
    enum Keys: String { case KURL = "urlt" }
    
    init?(_ json:Any?) {
        guard let object = json as? [String:Any],
            let url = object[Keys.KURL.rawValue] as? String else { return nil }
        self.url = url
    }
    required init?(coder aDecoder: NSCoder) {
        guard let url = aDecoder.decodeObject(forKey: Keys.KURL.rawValue) as? String else { return nil }
        self.url = url
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.url, forKey: Keys.KURL.rawValue)
    }
}

class RedditImages: Codable {
    let source: RedditSource
    
    enum Keys: String { case KSource = "source" }
    
    init?(_ json:Any?) {
        guard let objectList = json as? [[String:Any]],
            objectList.count > 0,
            let source = RedditSource(objectList[0]) else { return nil }
        self.source = source
    }
    required init?(coder aDecoder: NSCoder) {
        guard let source = aDecoder.decodeObject(forKey: Keys.KSource.rawValue) as? RedditSource else { return nil }
        self.source = source
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.source, forKey: Keys.KSource.rawValue)
    }
}


class RedditPreview: Codable {
    let images: RedditImages
    
    enum Keys: String { case KImages = "images" }
    
    init?(_ json:Any?) {
        guard let object = json as? [String:Any],
            let images = RedditImages(object[Keys.KImages.rawValue]) else { return nil }
        self.images = images
    }
    required init?(coder aDecoder: NSCoder) {
        guard let images = aDecoder.decodeObject(forKey: Keys.KImages.rawValue) as? RedditImages else { return nil }
        self.images = images
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.images, forKey: Keys.KImages.rawValue)
    }
}


class RedditChildren: Codable {
    let name:String
    let created:Int
    let author:String
    let numComments:Int
    var title:String?
    var thumbnail:String?
    var url:String?
    
    enum Keys: String {
        case KName = "name"
        case KCreated = "created_utc"
        case KAuthor = "author"
        case KNumComments = "num_comments"
        case KTitle = "title"
        case KThumbnail = "thumbnail"
        case KURL = "url"
    }
    
    init?(_ json:Any?) {
        guard let object = json as? [String:Any],
            let name = object[Keys.KName.rawValue] as? String,
            let created = object[Keys.KCreated.rawValue] as? Int,
            let author = object[Keys.KAuthor.rawValue] as? String,
            let numComments = object[Keys.KNumComments.rawValue] as? Int else { return nil }
        self.name = name
        self.created = created
        self.author = author
        self.numComments = numComments
        if let title = object[Keys.KTitle.rawValue] as? String { self.title = title }
        if let thumbnail = object[Keys.KThumbnail.rawValue] as? String { self.thumbnail = thumbnail }
        if let url = object[Keys.KURL.rawValue] as? String, url.suffix(4) == ".jpg" { self.url = url }
    }
    required init?(coder aDecoder: NSCoder) {
        guard let name = aDecoder.decodeObject(forKey: Keys.KName.rawValue) as? String else { return nil }
        guard let created = aDecoder.decodeObject(forKey: Keys.KCreated.rawValue) as? Int else { return nil }
        guard let author = aDecoder.decodeObject(forKey: Keys.KAuthor.rawValue) as? String else { return nil }
        guard let numComments = aDecoder.decodeObject(forKey: Keys.KNumComments.rawValue) as? Int else { return nil }
        self.name = name
        self.created = created
        self.author = author
        self.numComments = numComments
        self.title = aDecoder.decodeObject(forKey: Keys.KTitle.rawValue) as? String
        self.thumbnail = aDecoder.decodeObject(forKey: Keys.KThumbnail.rawValue) as? String
        self.url = aDecoder.decodeObject(forKey: Keys.KURL.rawValue) as? String
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.name, forKey: Keys.KName.rawValue)
        aCoder.encode(self.created, forKey: Keys.KCreated.rawValue)
        aCoder.encode(self.author, forKey: Keys.KAuthor.rawValue)
        aCoder.encode(self.numComments, forKey: Keys.KNumComments.rawValue)
        if let title = self.title { aCoder.encode(title, forKey: Keys.KTitle.rawValue) }
        if let thumbnail = self.thumbnail { aCoder.encode(thumbnail, forKey: Keys.KThumbnail.rawValue) }
        if let url = self.url { aCoder.encode(url, forKey: Keys.KURL.rawValue) }
    }
}

class RedditModel: Codable {
    private var __after:String?
    private var __childrenList:[RedditChildren]
    var after:String? { get { return self.__after } }
    var childrenList:[RedditChildren]  { get { return self.__childrenList }}
    
    enum Keys: String {
        case KAfter = "after"
        case KChildrenList = "childrenList"
    }
    
    init?(_ json:Any?) {
        guard let object = json as? [String:Any],
            let data = object["data"] as? [String:Any],
            let objectList = data["children"] as? [Any] else { return nil }
        if let after = data[Keys.KAfter.rawValue] as? String { self.__after = after }
        var childrenList = [RedditChildren]()
        for object in objectList {
            if let data = (object as? [String:Any])?["data"],
                let children = RedditChildren(data) {
                childrenList.append(children)
            }
        }
        self.__childrenList = childrenList
    }
    required init?(coder aDecoder: NSCoder) {
        guard let __after = aDecoder.decodeObject(forKey: Keys.KAfter.rawValue) as? String else { return nil }
        guard let __childrenList = aDecoder.decodeObject(forKey: Keys.KChildrenList.rawValue) as? [RedditChildren] else { return nil }
        self.__after = __after
        self.__childrenList = __childrenList
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.__after, forKey: Keys.KAfter.rawValue)
        aCoder.encode(self.__childrenList, forKey: Keys.KChildrenList.rawValue)
    }
    
    func union(redditModel:RedditModel) {
        guard redditModel.childrenList.count > 0 else { return }
        self.__after = redditModel.after
        self.__childrenList +=  redditModel.childrenList
    }
}
