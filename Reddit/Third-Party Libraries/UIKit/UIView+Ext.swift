//
//  UIView+Ext.swift
//  Reddit
//
//  Created by Gustavo Halperin on 11/7/14.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

import UIKit

/**
 File UIView+Ext.swift
 UIView extensions:
 #. Added IBInspectable to decorarte UIView, as border, rounded border, etc.
 #. Added func to add Subview in View Center,
 */

extension UIView {
    // MARK: IBActions
    @IBInspectable
    var cornerRadius:NSNumber! {
        get {
            return nil }
        set {
            if let radius = newValue {
                self.layer.cornerRadius = CGFloat(radius.int32Value) } } }
    
    @IBInspectable
    var border:UIColor! {
        get {
            return nil }
        set {
            if let color = newValue {
                self.layer.borderColor = color.cgColor
                self.layer.borderWidth = 0.5 } } }
    
    @IBInspectable
    var roundedBorder:UIColor! {
        get {
            return nil }
        set {
            if let color = newValue {
                self.layer.borderColor = color.cgColor
                self.layer.borderWidth = 1
                self.layer.cornerRadius = 4 } } }
    
    var roundedBorderRadious10:UIColor! {
        get {
            return nil }
        set {
            if let color = newValue {
                self.layer.borderColor = color.cgColor
                self.layer.borderWidth = 0.5
                self.layer.cornerRadius = 10 } } }
    
    @IBInspectable
    var circledBorder:UIColor! {
        get {
            return nil }
        set {
            if let color = newValue {
                self.layer.borderColor = color.cgColor
                self.layer.borderWidth = 0.5
                self.layer.cornerRadius = min(self.bounds.size.width, self.bounds.size.height) * 0.5
                self.layer.masksToBounds = true; } } }
    
    func addSubviewInCenter(view:UIView) {
        self.addSubview(view)
        guard let center = self.superview?.convert(self.center, to: self) else {
            view.center = self.center
            return }
        view.center = center }
    
}
