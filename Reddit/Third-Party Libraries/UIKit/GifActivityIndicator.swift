//
//  GifActivityIndicator.swift
//  Reddit
//
//  Created by Gustavo Halperin on 11/24/18.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

import UIKit
import ImageIO

/**
 class GifActivityIndicators
 Show and Hide Kind of custom Activity Animation View with custom gift.
 */

fileprivate struct GifActivityIndicator {
    public static var shared = [String:GifActivityIndicator]()
    
    private let container: UIView
    private let effectLoadingView: UIVisualEffectView
    private var redditImage:UIImage?
    init(dataName:String) {
        self.container = UIView()
        self.effectLoadingView = UIVisualEffectView(effect:UIBlurEffect(style:.light))
        self.redditImage = UIImage.animatedImage(dataName: dataName, duration: 0.8)
    }
    
    fileprivate func show(_ uiView: UIView) {
        self.removeFromSuperviews()
        
        self.container.frame = uiView.frame
        self.container.center = uiView.center
        self.container.backgroundColor = UIColor(rgbHex:0xffffff, alpha:0.45)
        
        self.effectLoadingView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        self.effectLoadingView.center = uiView.center
        self.effectLoadingView.clipsToBounds = true
        self.effectLoadingView.layer.cornerRadius = 10
        self.effectLoadingView.layer.borderColor = UIColor.black.cgColor
        self.effectLoadingView.layer.borderWidth = 1
        
        let imageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: 100.0, height: 100.0))
        imageView.image = self.redditImage
        self.effectLoadingView.contentView.addSubview(imageView)
        self.container.addSubview(effectLoadingView)
        uiView.addSubview(container)
    }
    
    fileprivate func removeFromSuperviews() {
        if var _ = self.container.superview {
            self.container.removeFromSuperview()
            self.effectLoadingView.removeFromSuperview()
        }
    }
}

/*
 Show customized activity indicator
 
 @param uiView - add activity indicator to this view
 **/
func ShowGifActivityIndicator(_ dataName:String) {
    if GifActivityIndicator.shared[dataName] == nil {
        GifActivityIndicator.shared[dataName] = GifActivityIndicator(dataName: dataName)
    }
    guard let activityIndicator = GifActivityIndicator.shared[dataName] else {
        assertionFailure("GifActivityIndicator with name \(dataName) doesn't exist.")
        return
    }
    dispatch_async_main {
        if let view = UIViewController.currentViewController()?.view {
            activityIndicator.show(view)
        }
    }
}
/*
 Hide activity indicator
 Actually remove activity indicator from its super view
 
 @param uiView - remove activity indicator from this view
 **/
func HideGifActivityIndicator(_ dataName:String) {
    guard let activityIndicator = GifActivityIndicator.shared[dataName] else { return }
    dispatch_async_main {
        activityIndicator.removeFromSuperviews()
    }
}
