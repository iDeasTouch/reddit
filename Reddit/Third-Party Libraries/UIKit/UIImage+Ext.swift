//
//  UIImage+Ext.swift
//  Reddit
//
//  Created by Gustavo Halperin on 11/25/18.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

import UIKit

/**
 File UIImage+Ext.swift
 UIImage extension
 Added load from gif file save as dat in Assets.
 BTW: Looks like this idea is not in the web.
 */

fileprivate extension CGImageSource {
    class func loadFromName(_ name:String) -> CGImageSource? {
        guard let dataAsset = NSDataAsset(name:name) else {
            assertionFailure("File couldn't load.")
            return nil
        }
        let data:CFData = dataAsset.data as CFData
        return CGImageSourceCreateWithData(data, nil)
    }
}

extension UIImage {
    class func animatedImage(dataName:String, duration:Double) -> UIImage? {
        guard let imageSource = CGImageSource.loadFromName(dataName) else {
            return nil
        }
        let count:size_t = CGImageSourceGetCount(imageSource)
        var images =  [UIImage]()
        for index in 0..<count {
            guard let image = CGImageSourceCreateImageAtIndex(imageSource, index, nil) else { continue }
            images.append(UIImage(cgImage:image))
        }
        return self.animatedImage(with: images, duration: duration)
    }
}
