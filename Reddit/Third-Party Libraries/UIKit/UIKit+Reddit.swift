//
//  UIFont+Reddit.swift
//  Reddit
//
//  Created by Gustavo Halperin on 11/24/18.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

import UIKit

/**
 File UIFont+Reddit.swift
 UIFont and UIColor extensions to support fonts and colors used later on by RedditCollectionViewCell.
 */

extension UIFont {
    static let redditTag = UIFont.systemFont(ofSize: 14, weight: .regular)
    static let redditTitle = UIFont(name: "TrebuchetMS", size: 21)
}

extension UIColor {
    static let redditTagBackground = UIColor.init(red: 245.0/250.0, green: 245.0/250.0, blue: 245.0/250.0, alpha: 0.45)
    static let redditTagStroke = UIColor.init(red: 10.0/250.0, green: 10.0/250.0, blue: 10.0/250.0, alpha: 1)
    static let redditTitleBackground = UIColor.init(red: 240.0/250.0, green: 240.0/250.0, blue: 240.0/250.0, alpha: 0.45)
    static let redditTitleStroke = UIColor.init(red: 5.0/250.0, green: 5.0/250.0, blue: 5.0/250.0, alpha: 1)
}
