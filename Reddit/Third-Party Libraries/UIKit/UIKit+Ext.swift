//
//  UIKit+Ext.swift
//  Reddit
//
//  Created by Gustavo Halperin on 11/24/18.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

import UIKit

/**
 File UIKit+Ext.swift
 UIColor extension to support init with number Hexadecimal.
 */

extension UIColor {
    convenience init(rgbHex:UInt32, alpha:CGFloat = 1.0) {
        let red = CGFloat((rgbHex & 0xFF0000) >> 16)/255.0
        let green = CGFloat((rgbHex & 0xFF00) >> 8)/255.0
        let blue = CGFloat(rgbHex & 0xFF)/255.0
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
