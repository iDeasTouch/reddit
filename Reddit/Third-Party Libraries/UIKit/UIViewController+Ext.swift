//
//  UIViewController+Ext.swift
//  Reddit
//
//  Created by Gustavo Halperin on 11/6/14.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

import UIKit

/**
 File UIViewController+Ext.swift
 UIViewController extensions:
 #. Function to dismiss self, kind of back action.
 #. Find Top most view controller.
 #. Present AlertView base on params as title, action, etc.
 */

extension UIViewController {
    // MARK: IBActions
    @IBAction func backAction(_ sender:Any? = nil) -> Void {
        self.dismiss(animated: true, completion: nil)
    }
    
    class func currentViewController() -> UIViewController? {
        if let viewController = UIApplication.shared.keyWindow?.rootViewController {
            return currentViewController(viewController) }
        else {
            return nil
        }
    }
    
    private class func currentViewController(_ viewController:UIViewController) -> UIViewController? {
        if let tabBarController = viewController as? UITabBarController {
            guard let selectedViewController = tabBarController.selectedViewController else {
                return nil
            }
            return currentViewController(selectedViewController)
        }
        if let presentedViewController = viewController.presentedViewController {
            return currentViewController(presentedViewController)
        }
        if let navigationController = viewController as? UINavigationController {
            return navigationController.topViewController
        }
        return viewController
    }
    
    func presentAlertViewWithAction(_ title:String?, message:String?, actionTitle:String, action:@escaping ()->Void) {
        let alert:UIAlertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title:actionTitle,
                                     style:UIAlertAction.Style.cancel,
                                     handler: { (UIAlertAction) in action() })
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil) }
    
    func presentAlertViewWithVoidAction(_ title:String?, message:String?, voidTitle:String) {
        self.presentAlertViewWithAction(title, message: message, actionTitle: voidTitle , action: {
            ()->Void in return }) }
    
    func presentAlertViewWithActionAndCancel(_ title:String?, message:String?, action:@escaping ()->Void) {
        let alert:UIAlertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title:"Ok",
                                     style:UIAlertAction.Style.default,
                                     handler: { (UIAlertAction) in action() })
        let cancelAction = UIAlertAction(title:"Cancel",
                                         style:UIAlertAction.Style.default,
                                         handler: { (UIAlertAction) in })
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil) }
}
