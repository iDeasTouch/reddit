//
//  RedditTests.swift
//  RedditTests
//
//  Created by Gustavo Halperin on 11/18/18.
//  Copyright © 2018 Gustavo Halperin. All rights reserved.
//

import XCTest
import Foundation
@testable import Reddit

/**
 Two test case implementation, one for one extension on string and one for test custom network class.
 */

class RedditTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testStringMessages() {
        XCTAssertEqual(String(1, postPendOne:"message", postPendMany:"messages"), "1 message")
        XCTAssertEqual(String(0, postPendOne:"message", postPendMany:"messages"), "0 messages")
        XCTAssertEqual(String(23_567, postPendOne:"message", postPendMany:"messages"), "23.6k messages")
        XCTAssertEqual(String(123, postPendOne:"message", postPendMany:"messages"), "123 messages")
        XCTAssertEqual(String(1_000, postPendOne:"message", postPendMany:"messages"), "1k messages")
        XCTAssertEqual(String(10_000, postPendOne:"message", postPendMany:"messages"), "10k messages")
    }
    /*
    func testRedditTop50() {
        var result:(redditModel:RedditModel?, error:Error?) = (nil,nil)
        let expectation = self.expectation(description: "Test Redit Top 50")
        RedditNetworkManager.shared.topPost(after: nil) { (json:Any?, error:Error?) in
            result.error = error
            if let json = json, let redditModel = RedditModel(json) {
                result.redditModel = redditModel
            }
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 3000, handler: nil)
        XCTAssertNil(result.error, "Error: \(String(describing: result.error))")
        XCTAssertNotNil(result.redditModel, "Result is nil")
    }
    */

}
