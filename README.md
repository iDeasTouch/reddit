# Reddit top entries app
A simple Reddit client that shows the top 50 entries fromwww.reddit.com/top.

The app show in each entry:
- Title (at its full length)
- Author
- entry date, following a format like “x hours ago”
- A thumbnail for those who have a picture.
- Number of comments

In addition, for each entry having a picture (besides the thumbnail),
 allows the user to tap on the thumbnail to be sent to the
full sized picture with option to save it in the Photo Album.

The app also include:
- Pagination support
- App state-preservation/restoration
- Support iPhone 5s and above.
- Support landscape and portrait (top orientation only)


------
------
# Use Instructions
- Clone from GitHub
- Create an app at Reddit for mobile uses with the same redirect url as in RedditNetworkManager.swift
- Write your client id Reddit App on client id at RedditNetworkManager.swift

------
------

# Full Technical documentation
## Main View Controllers
### Class RedditCollectionViewController
 It is a custom UICollectionViewController with RedditCollectionViewLayout as his custom UICollectionViewLayout.
 
#### Main functions:
 - Keep current RedditData model saved in UserDefaul.
 - Check current RedditData model in each apear instead of always fetch data from server.
 - Save current visible collection view index before rotation and scroll to it after rotation.
 - Save index of selectd collection view on selection and scroll when user back to collection view without matter if the device have or nor
   different orientation.
 - Always save index of the current vissible (unless there is a collection view selected) and restore this position in each new did appear.
 - Refresh data with the first 50 top post on swipe down from top.
 - Fetch the next 50 top messages when the the current last Collection View is about to be visible.
 
####Patterns:
 - MVC: View  <=> Controller  <=> Model
    - The NibFile share his view responsabilities with he RedditCollectionViewLayout to decide the postion and size of each Collection View.
 
### Class PictureViewController
 ViewController with Image that is loaded in realtime and option to save it in Photo Album.

### Class RedditAutorizationViewController
 ViewController with a WKWebView to handle authorization process without shows open URL with our private Reddit ID.
 
## Main Views and Models
### Class RedditCollectionViewCell
 Typical UICollectionViewCell included implementation of prepareForReuse.
### Class RedditCollectionViewLayout
 Custom UICollectionViewLayout to calculate the content sizes and their locations depend on the device screen and orientation.

## Custom App Third-Party Libraries
### Class RedditNetworkManager
 This class have the next main functionalities:
 - Assist RedditAuthorizationViewController in his steps to get authorization to Reddit server.
 - Save the token obtainer with RedditAuthorizationViewController, the expiration date and the refresh token in UserDefault.
 - Resolve autocamaticall expiration situation refreshing the tpken and calling recursivelly the initial function.
 - Fetch the first 50 top post starting by the after parameter.
 
#### Patterns:
 - Singlenton
 - Computer Variables
 - Recursion

### File RedditModel.swift
 In this file is defined the object representing the reddit data objecs.
 The Data structure of an object returned when using the reddit JSON API are in the link below:
   https://github.com/reddit-archive/reddit/wiki/JSON
 
#### Class main patterns:
 - Custom Model representing the values required to show values required in RedditCollectionViewCell & PictureViewController.
 - The Reddit JSON object is loaded with customs init from Any?.
 - Inheritance from Codable so the entirable model can be encoded and saved in DefaultUser and decoder when is read.
 
#### Patterns:
 - Custom build from Any?
 - Encode and Decode using Codable protocol

### File GifActivityIndicators+Reddit.swift
 Implement GifActivityIndicators with a gift from Reddit.

## Foundation Extensions
### File Foundation+Ext.swift
 - Extension of DispatchQueue to support block.
 - Extension NSObject to support init by dictionary using keys and values.
 
### File String+Ext.swift
 - Decorators to Int, String, and NSAttributedString later on used in RedditCollectionViewCell fields.

## UIKit Extensions
### File UIKit+Ext.swift
 - UIColor extension to support init with number Hexadecimal.
### File UIImage+Ext.swift
 UIImage extension:
 - Added load from gif file save as dat in Assets. ```Load from GIFt looks like a great idea.```
 
### File UIView+Ext.swift
#### UIView extensions:
 - Added IBInspectable to decorarte UIView, as border, rounded border, etc.
 - Added func to add Subview in View Center.
### File UIViewController+Ext.swift
 UIViewController extensions
 - Function to dismiss self, kind of back action.
 - Find Top most view controller.
 - Present AlertView base on params as title, action, etc.
 ### File UIKit+Reddit.swift
 - UIFont and UIColor extensions to support fonts and colors used later on by RedditCollectionViewCell.
 
## New Class
### Class GifActivityIndicators
 Show and Hide Kind of custom Activity Animation View with gif file.